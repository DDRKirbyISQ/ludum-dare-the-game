﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blinker : MonoBehaviour
{

    // Use this for initialization
    void Start() {
		
    }
	
    // Update is called once per frame
    void Update() {
        GetComponent<Text>().enabled = Mathf.Sin(Time.unscaledTime * 5) > 0;
    }
}
