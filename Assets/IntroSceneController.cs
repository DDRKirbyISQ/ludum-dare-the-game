﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HarmonicUnity;

public class IntroSceneController : MonoBehaviour
{
    [SerializeField]
    AudioClip _music;

    [SerializeField]
    AudioClip _bdhit;

    [SerializeField]
    GameObject _title;

    [SerializeField]
    GameObject _particles;

    [SerializeField]
    GameObject _text1;

    [SerializeField]
    GameObject _text2;

    [SerializeField]
    GameObject _text3;

    [SerializeField]
    Image _image;

    [SerializeField]
    ParticleSystem _particleSystem;

    [SerializeField]
    SpriteRenderer _spriteRenderer;

    bool _continue = false;

    // Use this for initialization
    void Start() {
        StartCoroutine(DoStart());
    }

    IEnumerator DoStart() {
        Genre.Genres = null;

        Resources.Load("HarmonicUnity/ScreenFlash");
        _title.SetActive(false);
        _particleSystem.GetComponent<Renderer>().material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, 0.0f));
        _spriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

        _particles.SetActive(true);
        _image.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

        yield return new WaitForSeconds(0.25f);
        AudioManager.PlaySound(_bdhit, 0.5f);
        _text1.SetActive(true);

        yield return new WaitForSeconds(1.5f);
        _text1.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        AudioManager.PlaySound(_bdhit, 0.5f);
        _text2.SetActive(true);

        yield return new WaitForSeconds(1.5f);
        _text2.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        AudioManager.PlaySound(_bdhit, 0.5f);
        _text3.SetActive(true);

        yield return new WaitForSeconds(2.0f);
        _text3.SetActive(false);
        yield return new WaitForSeconds(1.0f);
        AudioManager.PlayMusic(_music);
        yield return new WaitForSecondsRealtime(0.75f);

        _particleSystem.GetComponent<Renderer>().material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, 1.0f));
        _spriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);

        _image.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        _title.SetActive(true);

        _continue = true;

        yield break;
    }

    void Update() {
        Time.timeScale = Input.GetMouseButton(0) ? 5.0f : 1.0f;

        float a = _image.color.a;
        a -= Time.unscaledDeltaTime * 3;
        a = Mathf.Clamp01(a);
        _image.color = new Color(1.0f, 1.0f, 1.0f, a);

        if (Input.GetMouseButtonDown(0) && _continue) {
            AudioManager.PlaySound("powerup");
            SceneTransitioner.Transition("MainScene");
            Time.timeScale = 1.0f;
            enabled = false;
        }
    }
}
