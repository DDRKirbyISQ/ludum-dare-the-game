﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;
using UnityEngine.SceneManagement;

public class StartSceneController : MonoBehaviour
{

    // Use this for initialization
    void Update() {
        // Wait until splash screen is done.
        if (Application.isShowingSplashScreen) {
            return;
        }

        #if UNITY_IOS || UNITY_ANDROID
        Application.targetFrameRate = 60;
        #endif

        string[] allowedDomains = {
            "cocoamoss.com",
            "ddrkirby.com",
            "katmengjia.com",
            "localhost",
        };

        if (CopyProtection.HasViolation(allowedDomains)) {
            SceneManager.LoadScene("CopyProtectScene");
        } else {
            SceneManager.LoadScene("IntroScene");
        }

        enabled = false;
    }
}
