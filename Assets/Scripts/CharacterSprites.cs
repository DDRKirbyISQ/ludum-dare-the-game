﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSprites : MonoBehaviour
{
    public static CharacterSprites Instance;

    Dictionary<string, GameObject> _dict = new Dictionary<string, GameObject>();

    protected virtual void Awake() {
        Instance = this;
        for (int i = 0; i < transform.childCount; ++i) {
            GameObject child = transform.GetChild(i).gameObject;
            _dict.Add(child.name, child);
        }
    }

    public static void Switch(string name) {
        foreach (GameObject go in Instance._dict.Values) {
            if (go.name == name) {
                go.SetActive(true);
            } else {
                go.SetActive(false);
            }
        }
    }
}
