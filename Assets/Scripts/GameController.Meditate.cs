﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionMeditate() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("meditate");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("meditate");
        AudioManager.FadeMusic(1.0f / 2);

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
            AudioManager.PlayMusic("song1");
        };
        ActionName = "meditating";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;

        yield return new WaitForSeconds(2.0f);
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();
        yield return StartCoroutine(DialogBox.Show("Ohmmmmm~"));
        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();

        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 2.0f) {
                    GameController.Pause();


                    yield return StartCoroutine(DialogBox.Show("Ohmmmmm~"));
                    if (UnityEngine.Random.value < 0.5f) {
                        int energy = UnityEngine.Random.Range(1, 4);
                        int motivation = UnityEngine.Random.Range(1, 4);
                        GameController.Instance.Stats.CharacterEnergyInternal += energy;
                        GameController.Instance.Stats.CharacterMotivationInternal += motivation;
                        yield return StartCoroutine(ResultBox.Show("I understand the universe now.",
                            new[] {
                                string.Format("+{0} Energy", energy),
                                string.Format("+{0} Motivation", motivation)
                            },
                            new[] { "icon_energy", "icon_motivation" }
                        ));
                    } else {
                        yield return StartCoroutine(DialogBox.Show("...this was a stupid idea.  Oh well."));
                    }
                    CharacterSprites.Switch("computer");
                    GameController.StopAction(false);

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
