﻿using System.Text;
using System;
using System.Collections;
using System.Collections.Generic;
using HarmonicUnity;
using UnityEngine;
using UnityEngine.UI;

public class GenreBox : GenericBox
{
    public static GenreBox Instance;

    [SerializeField]
    GameObject _buttons;

    [SerializeField]
    GameObject _selectButton;

    int _selectedChoice = -1;

    bool _confirmed = false;

    Coroutine _displayCoroutine = null;

    public static IEnumerator Show() {
        Instance._selectedChoice = -1;
        Instance.gameObject.SetActive(true);
        Instance._selectButton.SetActive(false);
        Instance._buttons.SetActive(false);
        yield return Instance.StartCoroutine(Instance.ShowText("Select a genre to learn more about it:"));

        Instance._buttons.SetActive(true);
        while (!Instance._confirmed) {
            for (int i = 0; i < Instance._buttons.transform.childCount; ++i) {
                Instance._buttons.transform.GetChild(i).GetComponent<Image>().color = 
                    i == Instance._selectedChoice ? new Color(1.0f, 1.0f, Mathf.Sin(Time.unscaledTime * 5) / 2 + 0.5f) : Color.white;
            }

            yield return null;
        }
        Instance.gameObject.SetActive(false);
        yield break;
    }

    public void SelectChoice(int choice) {
        if (_selectedChoice == choice) {
            return;
        }

        _selectButton.SetActive(true);

        _selectedChoice = choice;
        if (_displayCoroutine != null) {
            StopCoroutine(_displayCoroutine);
        }
        _displayCoroutine = StartCoroutine(Instance.ShowText(Genre.Get(_selectedChoice).Description));
    }

    public void Confirm() {
        if (_selectedChoice == -1) {
            return;
        }
        GameController.Instance.Stats.Genre = Genre.Get(_selectedChoice);
        _confirmed = true;
    }

    protected override void Awake() {
        base.Awake();
        Instance = this;
    }
}
