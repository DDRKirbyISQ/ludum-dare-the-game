﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionAudio() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("audio");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("audio");

        Cleanup = () => {
            GameController.Instance.Stats.ProgressAudio /= 2;
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "working on audio";

        float rand = UnityEngine.Random.value;

        // Always takes some time to start working.
        yield return new WaitForSeconds(kWorkStartDelaySeconds);

        int choice = -1;

        while (true) {
            yield return null;
            GameController.Instance.Stats.ProgressAudio += Time.deltaTime;

            if (rand <= 0.5f) {
                // Normal feature.
                if (GameController.Instance.Stats.ProgressAudio >= 4.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressAudio = 0.0f;
                    GameController.StopAction(false);

                    int audio = Mathf.RoundToInt(20 * Stats.CharacterProductivity());
                    Stats.GameAudio += audio;
                    yield return ResultBox.Show(
                        "Success!  I finished a new song for my game!",
                        new[] { string.Format("+{0} Audio", audio) },
                        new[] { "icon_audio" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            } else if (rand <= 0.75f) {
                if (GameController.Instance.Stats.ProgressAudio >= 3.0f && choice == -1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    string[] list1 = {
                        "Burgers",
                        "Burritos",
                        "Frozen Pizza",
                        "Apple Pie",
                    };
                    string[] list2 = {
                        "Dynamite",
                        "Red Bull",
                        "Adrenaline",
                        "Electric Shocks",
                    };

                    string option1 = list1[UnityEngine.Random.Range(0, list1.Length)];
                    string option2 = list2[UnityEngine.Random.Range(0, list2.Length)];

                    float r = UnityEngine.Random.value;

                    if (r < 0.5f) {
                        yield return StartCoroutine(ChoiceBox.Show("I'm stuck...I need inspiration from something...", val => {
                            choice = val;
                        }, option1, option2));

                    } else {
                        yield return StartCoroutine(ChoiceBox.Show("I'm stuck...I need inspiration from something...", val => {
                            choice = val;
                        }, option2, option1));
                        choice = 1 - choice;
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                }

                if (GameController.Instance.Stats.ProgressAudio >= 6.0f && choice == 0) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressAudio = 0.0f;
                    GameController.StopAction(false);

                    int audio = Mathf.RoundToInt(30 * Stats.CharacterProductivity());
                    int food = UnityEngine.Random.Range(1, 3);
                    Stats.GameAudio += audio;
                    Stats.CharacterFoodInternal += food;
                    yield return ResultBox.Show(
                        "I feel strangely satisfied after writing that piece...",
                        new[] { string.Format("+{0} Audio", audio), string.Format("+{0} Food", food) },
                        new[] { "icon_audio", "icon_food" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }

                if (GameController.Instance.Stats.ProgressAudio >= 6.0f && choice == 1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressAudio = 0.0f;
                    GameController.StopAction(false);

                    int audio = Mathf.RoundToInt(30 * Stats.CharacterProductivity());
                    int energy = UnityEngine.Random.Range(1, 3);
                    Stats.GameAudio += audio;
                    Stats.CharacterEnergyInternal += energy;
                    yield return ResultBox.Show(
                        "This music really gets me pumped up!",
                        new[] { string.Format("+{0} Audio", audio), string.Format("+{0} Energy", energy) },
                        new[] { "icon_audio", "icon_energy" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }




            } else {
                // Normal feature.
                if (GameController.Instance.Stats.ProgressAudio >= 2.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressAudio = 0.0f;
                    GameController.StopAction(false);

                    int audio = Mathf.RoundToInt(10 * Stats.CharacterProductivity());
                    Stats.GameAudio += audio;
                    yield return ResultBox.Show(
                        "Success!  I finished some new sounds for my game!",
                        new[] { string.Format("+{0} Audio", audio) },
                        new[] { "icon_audio" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }

            }
        }
    }
}
