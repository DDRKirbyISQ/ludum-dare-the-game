﻿using System.Text;
using System;
using System.Collections;
using System.Collections.Generic;
using HarmonicUnity;
using UnityEngine;
using UnityEngine.UI;

public class BedBox : GenericBox
{
    public static BedBox Instance;

    [SerializeField]
    GameObject _buttons;

    int _selectedChoice = -1;

    public static IEnumerator Show(Action<int> action) {
        Instance._selectedChoice = -1;
        Instance.gameObject.SetActive(true);
        Instance._buttons.SetActive(false);
        yield return Instance.StartCoroutine(Instance.ShowText("How much should I sleep?"));

        Instance._buttons.SetActive(true);
        while (Instance._selectedChoice == -1) {
            yield return null;
        }
        Instance.gameObject.SetActive(false);
        action(Instance._selectedChoice);
        yield break;
    }

    public void SelectChoice(int choice) {
        _selectedChoice = choice;
    }

    protected override void Awake() {
        base.Awake();
        Instance = this;
    }
}
