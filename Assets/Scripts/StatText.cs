﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HarmonicUnity;

public class StatText : MonoBehaviour
{
    float _alpha = 1.2f;

    [SerializeField]
    AudioClip _sound;

    [SerializeField]
    Text _text;

    [SerializeField]
    Image _image;

    RectTransform _rectTransform;

    public static StatText Create(Transform parent, Vector2 position, string text, string spritePath) {
        Sprite sprite = Resources.Load<Sprite>(spritePath);
        StatText result = Utils.InstantiatePrefab<StatText>("StatText", parent);
        result._rectTransform = result.GetRequiredComponent<RectTransform>();
        result._rectTransform.anchoredPosition = position;
        result._text.text = text;
        result._image.sprite = sprite;
        AudioManager.PlaySound(result._sound);
        return result;
    }

    // Update is called once per frame
    void Update() {
        _rectTransform.anchoredPosition += new Vector2(0.0f, Time.unscaledDeltaTime * 15);

        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, _alpha);
        _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, _alpha);

        if (_alpha <= 0.0f) {
            Destroy(gameObject);
            return;
        }
        _alpha -= Time.unscaledDeltaTime;
    }
}
