﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionArt() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("drawing_normal");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("computer");

        Cleanup = () => {
            GameController.Instance.Stats.ProgressCode /= 2;
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "drawing";

        float rand = UnityEngine.Random.value;

        // Always takes some time to start working.
        yield return new WaitForSeconds(kWorkStartDelaySeconds);

        int choice = -1;


        while (true) {
            yield return null;
            GameController.Instance.Stats.ProgressArt += Time.deltaTime;

            if (rand <= 0.5f) {
                // Normal drawing.
                if (GameController.Instance.Stats.ProgressArt >= 4.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressArt = 0.0f;
                    GameController.StopAction(false);

                    int art = Mathf.RoundToInt(20 * Stats.CharacterProductivity());
                    Stats.GameArt += art;
                    yield return ResultBox.Show(
                        "Success!  I drew some more artwork for my game!",
                        new[] { string.Format("+{0} Art", art) },
                        new[] { "icon_art" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            } else if (rand <= 0.75f) {
                if (GameController.Instance.Stats.ProgressArt >= 3.0f && choice == -1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    string[] list1 = {
                        "Pink Princess",
                        "Medieval Knight",
                        "Cute Cat",
                        "Generic Dude",
                        "Fantasy Warrior"
                    };
                    string[] list2 = {
                        "Giant Mecha",
                        "Tiny Dinosaur",
                        "Oblong Triangle",
                        "Squiggly Line",
                        "Fat Eagle",
                        "Living Couch"
                    };

                    string option1 = list1[UnityEngine.Random.Range(0, list1.Length)];
                    string option2 = list2[UnityEngine.Random.Range(0, list2.Length)];

                    float r = UnityEngine.Random.value;

                    if (r < 0.5f) {
                        yield return StartCoroutine(ChoiceBox.Show("I need an idea for a character design...", val => {
                            choice = val;
                        }, option1, option2));

                    } else {
                        yield return StartCoroutine(ChoiceBox.Show("I need an idea for a character design...", val => {
                            choice = val;
                        }, option2, option1));
                        choice = 1 - choice;
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                }

                if (GameController.Instance.Stats.ProgressArt >= 6.0f && choice == 0) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressArt = 0.0f;
                    GameController.StopAction(false);

                    int art = Mathf.RoundToInt(20 * Stats.CharacterProductivity());
                    Stats.GameArt += art;
                    yield return ResultBox.Show(
                        "I finished my design!",
                        new[] { string.Format("+{0} Art", art) },
                        new[] { "icon_art" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
                    
                if (GameController.Instance.Stats.ProgressArt >= 6.0f && choice == 1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressArt = 0.0f;
                    GameController.StopAction(false);

                    int art = Mathf.RoundToInt(35 * Stats.CharacterProductivity());
                    Stats.GameArt += art;
                    yield return ResultBox.Show(
                        "This is super wacky...I love it!",
                        new[] { string.Format("+{0} Art", art) },
                        new[] { "icon_art" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }




            } else {
                if (GameController.Instance.Stats.ProgressArt >= 3.0f && choice == -1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    string[] list1 = {
                        "Space",
                        "Village",
                        "Underwater",
                        "Dark Cavern",
                        "Tall Grass",
                    };
                    string[] list2 = {
                        "Inside of Nose",
                        "Bathroom Rave",
                        "This Room",
                        "Above the Clouds",
                        "Alpha Centauri",
                    };

                    string option1 = list1[UnityEngine.Random.Range(0, list1.Length)];
                    string option2 = list2[UnityEngine.Random.Range(0, list2.Length)];

                    float r = UnityEngine.Random.value;

                    if (r < 0.5f) {
                        yield return StartCoroutine(ChoiceBox.Show("I need an idea for a background setting...", val => {
                            choice = val;
                        }, option1, option2));

                    } else {
                        yield return StartCoroutine(ChoiceBox.Show("I need an idea for a background setting...", val => {
                            choice = val;
                        }, option2, option1));
                        choice = 1 - choice;
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                }

                if (GameController.Instance.Stats.ProgressArt >= 6.0f && choice == 0) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressArt = 0.0f;
                    GameController.StopAction(false);

                    int art = Mathf.RoundToInt(30 * Stats.CharacterProductivity());
                    Stats.GameArt += art;
                    yield return ResultBox.Show(
                        "I finished the drawing!",
                        new[] { string.Format("+{0} Art", art) },
                        new[] { "icon_art" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }

                if (GameController.Instance.Stats.ProgressArt >= 6.0f && choice == 1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressArt = 0.0f;
                    GameController.StopAction(false);

                    int art = Mathf.RoundToInt(20 * Stats.CharacterProductivity());
                    int motivation = UnityEngine.Random.Range(1, 4);
                    Stats.GameArt += art;
                    Stats.CharacterMotivationInternal += motivation;
                    yield return ResultBox.Show(
                        "This is super fun!",
                        new[] { string.Format("+{0} Art", art), string.Format("+{0} Motivation", motivation) },
                        new[] { "icon_art", "icon_motivation" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }





            }
        }
    }
}
