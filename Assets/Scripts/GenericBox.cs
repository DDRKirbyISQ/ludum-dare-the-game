﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using HarmonicUnity;
using UnityEngine;
using UnityEngine.UI;

public class GenericBox : MonoBehaviour
{
    public const float kTextSpeed = 0.03f;

    [SerializeField]
    Text _text;

    [SerializeField]
    AudioClip _sound;

    float _timeToWait = 0.0f;

    protected IEnumerator ShowText(string text) {
        StringBuilder stringBuilder = new StringBuilder();

        _timeToWait = 0.0f;

        for (int i = 0; i < text.Length; ++i) {
            stringBuilder.Append(text[i]);

            if (text[i] == '<') {
                while (text[i] != '/') {
                    ++i;
                    stringBuilder.Append(text[i]);
                }
                while (text[i] != '>') {
                    ++i;
                    stringBuilder.Append(text[i]);
                }
            }

            _text.text = stringBuilder.ToString();
            AudioManager.PlaySound(_sound, 0.5f);

            _timeToWait += kTextSpeed;

            while (_timeToWait > 0.0f) {
                yield return null;

                if (Input.GetMouseButtonDown(0)) {
                    _text.text = text;
                    yield return null;
                    yield break;
                }



                _timeToWait -= Time.unscaledDeltaTime;
            }
        }

        yield break;
    }

    protected virtual void Awake() {
        gameObject.SetActive(false);
    }
}
