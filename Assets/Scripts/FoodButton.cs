﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using HarmonicUnity;

public class FoodButton : MyButton, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData) {
        StartCoroutine(DoOnPointerClick());
    }

    IEnumerator DoOnPointerClick() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        int result = -1;
        yield return StartCoroutine(FoodBox.Show("What should I have...?", val => {
            result = val;
        }));

        if (result == 4) {
            GameController.Unpause();
            DefaultCanvas.ReleaseDialogLock();

            yield break;
        }

        int stop = -1;
        yield return StartCoroutine(ChoiceBox.AskStopAction(val => {
            stop = val;
        }));

        if (stop == 1) {
            switch (result) {
            case 0:
                yield return StartCoroutine(DialogBox.Show("I'll just microwave some pizza."));
                GameController.ChangeAction(GameController.GameAction.Pizza);
                break;
            case 1:
                yield return StartCoroutine(DialogBox.Show("I'll cook myself a proper meal!"));
                GameController.ChangeAction(GameController.GameAction.Cook);
                break;
            case 2:
                yield return StartCoroutine(DialogBox.Show("Let me pour myself some coffee..."));
                GameController.ChangeAction(GameController.GameAction.Coffee);
                break;
            case 3:
                yield return StartCoroutine(DialogBox.Show("Let me open up a beer from the fridge..."));
                GameController.ChangeAction(GameController.GameAction.Beer);
                break;
            default:
                Utils.Assert(false);
                break;
            }
        }

        GameController.Unpause();
        DefaultCanvas.ReleaseDialogLock();

        yield break;
    }
}
