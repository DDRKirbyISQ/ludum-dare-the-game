﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionBeer() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("beer");
        audioSource.Play();
        CharacterSprites.Switch("fridge");

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "drinking beer";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;
        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 1.5f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    GameController.StopAction(false);

                    AudioManager.PlaySound("swallow");
                    yield return StartCoroutine(DialogBox.Show("*Guzzle*..."));
                    if (UnityEngine.Random.value < 0.5f) {
                        int energy = UnityEngine.Random.Range(1, 4);
                        GameController.Instance.Stats.CharacterEnergyInternal -= energy;
                        yield return StartCoroutine(ResultBox.Show("I'm starting to feel a little woozy...",
                            new[] { string.Format("<color=#ff0000>-{0} Energy</color>", energy) },
                            new[] { "icon_energy" }
                        ));
                        GameController.Instance.StartCoroutine(GameController.Instance.DoDrunk());
                    } else {
                        int motivation = UnityEngine.Random.Range(1, 4);
                        GameController.Instance.Stats.CharacterMotivationInternal += motivation;
                        yield return StartCoroutine(ResultBox.Show("Wow, that really hit the spot!",
                            new[] { string.Format("+{0} Motivation", motivation) },
                            new[] { "icon_motivation" }
                        ));
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
