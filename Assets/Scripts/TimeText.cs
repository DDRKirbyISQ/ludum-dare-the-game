﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeText : MonoBehaviour
{
    Text _text;

    void Awake() {
        _text = GetComponent<Text>();
    }

    void Update() {
        int hours = Mathf.FloorToInt(GameController.Instance.TimeLeft);
        int minutes = Mathf.FloorToInt((GameController.Instance.TimeLeft - hours) * 60);
        //int seconds = Mathf.FloorToInt((GameController.Instance.TimeLeft - hours - minutes / 60.0f) * 3600);

        StringBuilder builder = new StringBuilder("Time remaining:\n");
        builder.Append(hours.ToString("D2"));
        builder.Append("hr ");
        builder.Append(minutes.ToString("D2"));
        builder.Append("min");
//        builder.Append(seconds.ToString("D2"));
        //builder.Append("sec ");
        _text.text = builder.ToString();
    }
}
