﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    SpriteRenderer _spriteRenderer;

    float _startTime = 0.0f;

    bool _glowing = false;

    public bool ForceGlow = false;

    public void OnPointerEnter(PointerEventData eventData) {
        _glowing = true;
        _startTime = Time.unscaledTime;
    }

    public void OnPointerExit(PointerEventData eventData) {
        _glowing = false;
    }

    protected virtual void Update() {
        if (ForceGlow || (_glowing && !GameController.Instance.Paused)) {
            float val = -Mathf.Cos((Time.unscaledTime - _startTime) * 10) * 0.25f + 0.75f;
            _spriteRenderer.color = new Color(val, val, val);
        } else {
            _spriteRenderer.color = Color.white;
        }
    }
}
