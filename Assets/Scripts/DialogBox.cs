﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using HarmonicUnity;
using UnityEngine;
using UnityEngine.UI;

public class DialogBox : GenericBox
{
    public static DialogBox Instance;

    [SerializeField]
    Image _image;

    public static IEnumerator Show(string text) {
        Instance.gameObject.SetActive(true);
        yield return Instance.StartCoroutine(Instance.ShowText(text));
        while (!Input.GetMouseButtonDown(0)) {
            yield return null;
        }
        Instance.gameObject.SetActive(false);
        yield break;
    }

    protected override void Awake() {
        base.Awake();
        Instance = this;
    }
}
