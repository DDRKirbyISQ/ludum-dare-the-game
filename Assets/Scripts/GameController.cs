﻿using System;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    public enum GamePhase
    {
        Intro,
        Prepare,
        Brainstorm,
        Main,
    }

    public enum GameAction
    {
        Code,
        Art,
        Audio,
        Browse,
        Bugs,
        Design,
        Post,
        Pizza,
        Cook,
        Coffee,
        Beer,
        Meditate,
        Nap,
        Sleep,
        Bounce,
    }

    public static GameController Instance;

    [NonSerialized]
    public Stats Stats = new Stats();

    [NonSerialized]
    public GamePhase Phase = GamePhase.Intro;

    [NonSerialized]
    public bool TutorialEnabled = true;

    [NonSerialized]
    public bool Paused = true;

    [NonSerialized]
    public Coroutine CurrentAction = null;

    [NonSerialized]
    public Action Cleanup = null;

    [NonSerialized]
    public string ActionName = "";

    [NonSerialized]
    public float TimeLeft = 48.0f;

    [NonSerialized]
    public bool CoffeeCrash = false;

    // 48hr is 2880 min.
    // We want 2 min.
    public const float kTimeScale = 2880.0f / 2;

    public const float kWorkStartDelaySeconds = 0.01f;

    [SerializeField]
    Transform _computerPosition;

    [SerializeField]
    MyButton _computerButton;

    [SerializeField]
    MyButton _foodButton;

    [SerializeField]
    MyButton _designButton;

    [SerializeField]
    BedButton _bedButton;

    [SerializeField]
    BedButton2 _bedButton2;

    [SerializeField]
    GameObject _needsGlow;

    [SerializeField]
    GameObject _categoriesGlow;

    [SerializeField]
    GameObject _requirementsGlow;

    [SerializeField]
    GameObject _timeGlow;

    public static void Pause() {
        Instance.Paused = true;
        Time.timeScale = 0.0f;
    }

    public static void Unpause() {
        Instance.Paused = false;
        Time.timeScale = 1.0f;
    }

    public static void StopAction(bool stopCoroutine = true) {
        if (Instance.CurrentAction != null && stopCoroutine) {
            Instance.StopCoroutine(Instance.CurrentAction);
        }
        Instance.CurrentAction = null;
        Instance.ActionName = "";
        if (Instance.Cleanup != null) {
            Instance.Cleanup();
        }
        Instance.Cleanup = null;
    }

    public static void ChangeAction(GameAction action) {
        StopAction();

        switch (action) {
        case GameAction.Code:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionCode());
            break;
        case GameAction.Art:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionArt());
            break;
        case GameAction.Audio:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionAudio());
            break;
        case GameAction.Browse:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionBrowse());
            break;
        case GameAction.Post:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionPost());
            break;
        case GameAction.Design:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionDesign());
            break;
        case GameAction.Bugs:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionBugs());
            break;
        case GameAction.Coffee:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionCoffee());
            break;
        case GameAction.Pizza:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionPizza());
            break;
        case GameAction.Cook:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionCook());
            break;
        case GameAction.Beer:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionBeer());
            break;
        case GameAction.Bounce:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionBounce());
            break;
        case GameAction.Meditate:
            Instance.CurrentAction = Instance.StartCoroutine(Instance.GameActionMeditate());
            break;
        default:
            Utils.Assert(false);
            break;
        }
    }

    protected virtual void Awake() {
        Instance = this;
    }

    protected virtual void Start() {
        StartCoroutine(DoStart());
    }

    protected virtual void Update() {
        if (Paused) {
            return;
        }

        #if UNITY_EDITOR
        if (Input.GetKey(KeyCode.Space)) {
            Time.timeScale = 5.0f;
        }
        if (Input.GetKey(KeyCode.E)) {
            TimeLeft = 0.5f;
        }
        #endif


        TimeLeft -= Time.deltaTime / 3600 * kTimeScale;

        if (TimeLeft <= 0.0f) {
            Pause();
            StartCoroutine(DoEnd());
        }
    }

    IEnumerator DoEnd() {
        TimeLeft = 0.0f;
        yield return DefaultCanvas.GetDialogLock();
        TimeLeft = 0.0f;
        GameController.Pause();

        StopAction();

        AudioManager.PlaySound("whistle");
        yield return StartCoroutine(TextBox.Show("Time's up!"));

        yield return StartCoroutine(TextBox.Show("Let's see how you did on your game!"));

        if (Stats.Genre.MeetsRequirements()) {
            yield return StartCoroutine(TextBox.Show("It looks like you managed to meet all of the <color=#ff80ff>requirements</color> for your game genre.  Good job!"));
        } else {
            yield return StartCoroutine(TextBox.Show("Unfortunately, you weren't able to meet all of the <color=#ff80ff>requirements</color> for your game genre.  Better luck next time!"));
        }

        yield return StartCoroutine(TextBox.Show("Now it's time to rest and wait for other jammers to rate your game!"));

        AudioManager.FadeMusic(1.0f);
        ScreenFader.FadeOut(3.0f);
        AudioManager.PlaySound("sleep");
        yield return new WaitForSecondsRealtime(4.0f);
        ScreenFader.FadeIn(3.0f);
        yield return new WaitForSecondsRealtime(2.0f);
        AudioManager.PlayMusic("song1");
        yield return new WaitForSecondsRealtime(1.0f);

        yield return StartCoroutine(TextBox.Show("It's been some time -- let's see what people think of your game!"));

        List<string> comments = new List<string>();
        int highest = 125;
        int high = 75;
        int low = 49;
        if (Stats.GameArt >= highest) {
            comments.Add("\"You seriously hand-animated every character in 120FPS??  Amazing.\"");
            comments.Add("\"This game is so beautiful it makes me want to cry.\"");
            comments.Add("\"I wish I had your drawing talent *cries*\"");
            comments.Add("\"You must have used 100000 layers in photoshop to make these graphics!!!1!1!!!.\"");
        }
        if (Stats.GameArt >= high) {
            comments.Add("\"Wow, wonderful graphics!\"");
            comments.Add("\"Pretty!\"");
            comments.Add("\"The animations in this game are pretty good.\"");
        }
        if (Stats.GameArt <= low) {
            comments.Add("\"The art wasn't that good...\"");
            comments.Add("\"I wish I could like it but I couldn't actually tell what anything was on the screen.\"");
            comments.Add("\"Looks like it was drawn by a five-year-old...\"");
        }
        if (Stats.GameProgramming >= highest) {
            comments.Add("\"I feel like this was made by a AAA studio O_o\"");
            comments.Add("\"Procedurally-generated multi-dimensional fractal spawners???  How do you have time to program all of this?\"");
            comments.Add("\"SO MANY FEATURES!\"");
            comments.Add("\"Are you a coding robot or something???\"");
        }
        if (Stats.GameProgramming >= high) {
            comments.Add("\"Great job implementing your ideas!\"");
            comments.Add("\"I can't believe you managed to code all of this in just 48h!\"");
        }
        if (Stats.GameProgramming <= low) {
            comments.Add("\"Seemed like a pretty easy game to make.\"");
            comments.Add("\"There weren't that many features implemented...\"");
            comments.Add("\"I wanted to like it, but it was too short.\"");
        }
        if (Stats.GameAudio >= highest) {
            comments.Add("\"WHERE CAN I BUY THE SOUNDTRACK\"");
            comments.Add("\"Can't vote, too busy jamming out to this entry\"");
            comments.Add("\"My ears...they are in heaven.\"");
            comments.Add("\"Such beautiful music...I want it to be played at my funeral...\"");
        }
        if (Stats.GameAudio >= high) {
            comments.Add("\"What program did you use to make these awesome beats???\"");
            comments.Add("\"I would play this game just to listen to the music alone.\"");
        }
        if (Stats.GameAudio <= 0) {
            comments.Add("\"It was kind of boring since there was no music...\"");
            comments.Add("\"Add some sound next time!\"");
            comments.Add("\"No audio...\"");
        }
        if (Stats.GameAudio > 0 && Stats.GameAudio <= low) {
            comments.Add("\"The music got really annoying after a while.\"");
            comments.Add("\"The sound could use some work.\"");
        }
        if (Stats.GameDesign >= highest) {
            comments.Add("\"This game blew my mind!  Who thought gravity chambers could be so fun?\"");
            comments.Add("\"The number of possibilities in this game is amazing.\"");
            comments.Add("\"Best gameplay by FAR.\"");
            comments.Add("\"*THIS* is innovation.\"");
        }
        if (Stats.GameDesign >= high) {
            comments.Add("\"A pretty fresh take on the genre!\"");
            comments.Add("\"Very well designed.\"");
        }
        if (Stats.GameDesign <= low) {
            comments.Add("\"The gameplay was boring.\"");
            comments.Add("\"Sorry, I didn't really have much fun playing your game.\"");
            comments.Add("\"I couldn't really figure out what to do?\"");
        }
        if (Stats.GameBugs >= 5) {
            comments.Add("\"I tried to run the game but it kept crashing my computer.  Also my CD drive kept on ejecting, how did you do that?\"");
            comments.Add("\"I couldn't move to the left.  Maybe a bug?  It was an interesting challenge trying to finish the game that way.\"");
            comments.Add("\"I couldn't get past all the bugs.\"");
            comments.Add("\"The game kept glitching out so all I saw were these swirls of color???  It was pretty dope though, A+ would watch while tripping again.\"");
        }
        if (Stats.GameBugs >= 3) {
            comments.Add("\"I ran into this bug once where the character kept on flying upwards into space.\"");
        }
        if (Stats.GameBugs >= 1) {
            comments.Add("\"There was this weird bug where I kept on clipping through the floor.\"");
        }


        float finalScore = (Stats.GameArt + Stats.GameAudio + Stats.GameDesign + Stats.GameProgramming + Stats.GameCommunity) / 5;
        if (Stats.GameProgramming >= 100) {
            finalScore += 10;
        }
        if (Stats.GameArt >= 100) {
            finalScore += 10;
        }
        if (Stats.GameAudio >= 100) {
            if (Stats.Genre.Index == 2) {
                finalScore += 30;
                finalScore = Mathf.Max(finalScore, 0);
            } else {
                finalScore += 10;
            }
        }
        if (Stats.GameDesign >= 100) {
            finalScore += 10;
        }
        if (Stats.GameCommunity >= 100) {
            finalScore += 10;
        }

        if (Stats.GameProgramming < 50) {
            finalScore -= 10;
            finalScore = Mathf.Max(finalScore, 0);
        }
        if (Stats.GameArt < 50) {
            finalScore -= 10;
            finalScore = Mathf.Max(finalScore, 0);
        }
        if (Stats.GameAudio < 50) {
            if (Stats.Genre.Index == 2) {
                finalScore -= 30;
                finalScore = Mathf.Max(finalScore, 0);
            } else {
                finalScore -= 10;
                finalScore = Mathf.Max(finalScore, 0);
            }
        }
        if (Stats.GameDesign < 50) {
            finalScore -= 10;
            finalScore = Mathf.Max(finalScore, 0);
        }
        if (Stats.GameCommunity < 50) {
            finalScore -= 10;
            finalScore = Mathf.Max(finalScore, 0);
        }

        if (Stats.GameBugs <= 0) {
        } else {
            finalScore -= 5 * Stats.GameBugs;
            finalScore = Mathf.Max(finalScore, 0);
        }

        if (!Stats.Genre.MeetsRequirements()) {
            finalScore /= 2;
        }

        if (finalScore >= highest) {
            comments.Add("\"#1 game of LD.\"");
            comments.Add("\"This is the best game I have ever played.\"");
            comments.Add("\"This game's power level is OVER 9000!!!\"");
            comments.Add("\"6 out of 5 stars!\"");
        }
        if (finalScore >= high) {
            comments.Add("\"Great game overall!\"");
            comments.Add("\"I really liked this game!\"");
        }
        if (finalScore <= low) {
            comments.Add("\"Game wasn't really that great overall.\"");
            comments.Add("\"Try better next time.\"");
        }

        // Community is rated from 0 to 100

        float communityLevel = Stats.GameCommunity / 100.0f;
        communityLevel = Mathf.Clamp(communityLevel, 0, 1);

        // num votes is from 5 to 20.
        int numVotes = Mathf.RoundToInt(5 + communityLevel * 15.0f);

        yield return StartCoroutine(TextBox.Show(string.Format("Your <color=#ffff80>community</color> score was {0}.  Looks like that garnered you {1} votes!", Stats.GameCommunity, numVotes)));

        yield return StartCoroutine(TextBox.Show("Let's take a look at what people said..."));

        if (numVotes > comments.Count)
            numVotes = comments.Count;

        int numNeeded = numVotes;
        int numLeft = comments.Count;
        List<string> realComments = new List<string>();
        foreach (string comment in comments) {
            if (UnityEngine.Random.value < (float)numNeeded / numLeft) {
                realComments.Add(comment);
                numNeeded--;
                numLeft--;
            }
        }

        foreach (string comment in realComments) {
            yield return StartCoroutine(TextBox.Show(comment));
        }

        if (Stats.GameCommunity <= low) {
            yield return StartCoroutine(TextBox.Show("Try getting a better <color=#ffff80>community</color> score next time to get more feedback!"));
        }
        if (Stats.GameCommunity >= high) {
            yield return StartCoroutine(TextBox.Show("Wow!  You had a lot of people excited to rate and play your game!  Good job garnering interest!"));
        }




        finalScore = (Stats.GameArt + Stats.GameAudio + Stats.GameDesign + Stats.GameProgramming + Stats.GameCommunity) / 5;
        yield return StartCoroutine(TextBox.Show("Now let's calculate your overall rating!"));
        yield return StartCoroutine(TextBox.Show(string.Format("The average of your scores for <color=#ffff80>Programming</color>, <color=#ffff80>Art</color>, <color=#ffff80>Audio</color>, <color=#ffff80>Design</color>, and <color=#ffff80>Community</color> was {0}.", Mathf.RoundToInt(finalScore))));
        if (finalScore > 100) {
            yield return StartCoroutine(TextBox.Show("That's amazing!"));
        } else if (finalScore > 66) {
            yield return StartCoroutine(TextBox.Show("That's pretty good!"));
        } else if (finalScore > 33) {
            yield return StartCoroutine(TextBox.Show("That's decent!"));
        } else {
            yield return StartCoroutine(TextBox.Show("That doesn't really seem too great..."));
        }

        if (Stats.GameProgramming >= 100) {
            finalScore += 10;
            yield return StartCoroutine(TextBox.Show(string.Format("You did exceptionally well in <color=#80ff80>Programming</color>, which gets you 10 extra points for a total of {0}!", Mathf.RoundToInt(finalScore))));
        }
        if (Stats.GameArt >= 100) {
            finalScore += 10;
            yield return StartCoroutine(TextBox.Show(string.Format("You did exceptionally well in <color=#80ff80>Art</color>, which gets you 10 extra points for a total of {0}!", Mathf.RoundToInt(finalScore))));
        }
        if (Stats.GameAudio >= 100) {
            if (Stats.Genre.Index == 2) {
                finalScore += 30;
                finalScore = Mathf.Max(finalScore, 0);
                yield return StartCoroutine(TextBox.Show(string.Format("You did exceptionally well in <color=#80ff80>Audio</color>, which really helps for a <color=#ffff00>Rhythm game</color>!")));
                yield return StartCoroutine(TextBox.Show(string.Format("That's a bonus of 30 points for a total of {0}...", Mathf.RoundToInt(finalScore))));
            } else {
                finalScore += 10;
                yield return StartCoroutine(TextBox.Show(string.Format("You did exceptionally well in <color=#80ff80>Audio</color>, which gets you 10 extra points for a total of {0}!", Mathf.RoundToInt(finalScore))));
            }
        }
        if (Stats.GameDesign >= 100) {
            finalScore += 10;
            yield return StartCoroutine(TextBox.Show(string.Format("You did exceptionally well in <color=#80ff80>Design</color>, which gets you 10 extra points for a total of {0}!", Mathf.RoundToInt(finalScore))));
        }
        if (Stats.GameCommunity >= 100) {
            finalScore += 10;
            yield return StartCoroutine(TextBox.Show(string.Format("You did exceptionally well in <color=#80ff80>Community</color>, which gets you 10 extra points for a total of {0}!", Mathf.RoundToInt(finalScore))));
        }

        if (Stats.GameProgramming < 50) {
            finalScore -= 10;
            finalScore = Mathf.Max(finalScore, 0);
            yield return StartCoroutine(TextBox.Show(string.Format("You were below average in <color=#ff8080>Programming</color>, so you got docked by 10 points for a total of {0}...", Mathf.RoundToInt(finalScore))));
        }
        if (Stats.GameArt < 50) {
            finalScore -= 10;
            finalScore = Mathf.Max(finalScore, 0);
            yield return StartCoroutine(TextBox.Show(string.Format("You were below average in <color=#ff8080>Art</color>, so you got docked by 10 points for a total of {0}...", Mathf.RoundToInt(finalScore))));
        }
        if (Stats.GameAudio < 50) {
            if (Stats.Genre.Index == 2) {
                finalScore -= 30;
                finalScore = Mathf.Max(finalScore, 0);
                yield return StartCoroutine(TextBox.Show(string.Format("You were below average in <color=#ff8080>Audio</color>, which really hurts for a <color=#ffff00>Rhythm game</color>!")));
                yield return StartCoroutine(TextBox.Show(string.Format("That's a penalty of 30 points for a total of {0}...", Mathf.RoundToInt(finalScore))));
            } else {
                finalScore -= 10;
                finalScore = Mathf.Max(finalScore, 0);
                yield return StartCoroutine(TextBox.Show(string.Format("You were below average in <color=#ff8080>Audio</color>, so you got docked by 10 points for a total of {0}...", Mathf.RoundToInt(finalScore))));
            }
        }
        if (Stats.GameDesign < 50) {
            finalScore -= 10;
            finalScore = Mathf.Max(finalScore, 0);
            yield return StartCoroutine(TextBox.Show(string.Format("You were below average in <color=#ff8080>Design</color>, so you got docked by 10 points for a total of {0}...", Mathf.RoundToInt(finalScore))));
        }
        if (Stats.GameCommunity < 50) {
            finalScore -= 10;
            finalScore = Mathf.Max(finalScore, 0);
            yield return StartCoroutine(TextBox.Show(string.Format("You were below average in <color=#ff8080>Community</color>, so you got docked by 10 points for a total of {0}...", Mathf.RoundToInt(finalScore))));
        }

        if (Stats.GameBugs <= 0) {
            yield return StartCoroutine(TextBox.Show(string.Format("You actually managed to make a bug-free game...impressive!")));
        } else {
            if (Stats.Genre.Index == 1) {
                finalScore -= 10 * Stats.GameBugs;
                finalScore = Mathf.Max(finalScore, 0);
                yield return StartCoroutine(TextBox.Show(string.Format("Looks like you had some <color=#ff8080>bugs</color> in your game...that's not good for an <color=#ffff00>Arcade game</color>!")));
                yield return StartCoroutine(TextBox.Show(string.Format("You had {0} <color=#ff8080>bug(s)</color> in your game...so you had {1} points taken off for that, for a total of {2}...", Stats.GameBugs, Stats.GameBugs * 5, Mathf.RoundToInt(finalScore))));
            } else {
                finalScore -= 5 * Stats.GameBugs;
                finalScore = Mathf.Max(finalScore, 0);
                yield return StartCoroutine(TextBox.Show(string.Format("Looks like you had {0} <color=#ff8080>bug(s)</color> in your game...you had {1} points taken off for that, for a total of {2}...", Stats.GameBugs, Stats.GameBugs * 5, Mathf.RoundToInt(finalScore))));
            }
        }

        if (!Stats.Genre.MeetsRequirements()) {
            yield return StartCoroutine(TextBox.Show("Unfortunately, you didn't meet the requirements to finish your game, so you get a <color=#ff0000>50% penalty</color>."));
            finalScore /= 2;
            yield return StartCoroutine(TextBox.Show(string.Format("That puts your score at {0}.", Mathf.RoundToInt(finalScore))));
        }

        yield return StartCoroutine(TextBox.Show(string.Format("So your final overall rating was: <color=#ffff80>{0}</color>!", Mathf.RoundToInt(finalScore))));

        yield return StartCoroutine(TextBox.Show("Thanks for participating in Ludum Dare!  See you next time!"));

        ScreenFader.FadeOut(3.0f);
        AudioManager.FadeMusic(1.0f / 3.0f);
        yield return new WaitForSecondsRealtime(3.0f);
        ScreenFader.FadeIn(1.0f);
        SceneManager.LoadScene("IntroScene");
        DefaultCanvas.ReleaseDialogLock();
        yield break;
    }

    public IEnumerator DoCoffeeCrash() {
        yield return new WaitForSeconds(20.0f);

        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();
        int energy = UnityEngine.Random.Range(4, 6);
        GameController.Instance.Stats.CharacterEnergyInternal -= energy;
        yield return StartCoroutine(ResultBox.Show("I'm starting to crash from that caffeine high I had earlier...",
            new[] { string.Format("<color=#ff0000>-{0} Energy</color>", energy) },
            new[] { "icon_energy" }
        ));
        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
    }

    public IEnumerator DoDrunk() {
        yield return new WaitForSeconds(2.0f);

        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();
        yield return StartCoroutine(DialogBox.Show("*Hic*...eheh...silly Loodum Daaahhhraayyy..."));
        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();

        yield return new WaitForSeconds(1.0f);

        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();
        yield return StartCoroutine(DialogBox.Show("*Hic*...or is it Luhdumb Dheerr..."));
        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();

        yield return new WaitForSeconds(2.0f);

        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();
        yield return StartCoroutine(DialogBox.Show("*Hic*...why'z the walls all spinny..."));
        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();

        yield return new WaitForSeconds(2.0f);

        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();
        yield return StartCoroutine(DialogBox.Show("*Hic*..."));
        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
        yield break;
    }

    IEnumerator DoStart() {
        CharacterSprites.Switch("computer");
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();
        AudioManager.PlayMusic("song1");

        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        if (true) {

            yield return StartCoroutine(TextBox.Show("Welcome to <color=#ffff80>Ludum Dare: The Game!</color>"));

            int result = -1;
            yield return StartCoroutine(ChoiceBox.Show("Is this your first time playing?", val => {
                result = val;
            }));
            TutorialEnabled = result == 1;

            if (TutorialEnabled) {
                yield return StartCoroutine(TextBox.Show("Welcome to your first Ludum Dare!  In this event you'll be trying your best to make an entire game in just 48 hours!"));
            }

            yield return StartCoroutine(TextBox.Show("The theme is about to be announced!  Let's check out the LD website to make sure we see it as soon as it comes out!"));

            yield return StartCoroutine(RefreshBox.Show());

            yield return StartCoroutine(DialogBox.Show("...Looks like the website crashed...again..."));

            yield return StartCoroutine(DialogBox.Show("I'll try checking Twitter instead."));

            yield return StartCoroutine(TextBox.Show("The theme for this Ludum Dare is..."));

            Stats.Theme = ThemeGenerator.GetRandom();

            yield return StartCoroutine(TextBox.Show(string.Format("<color=#ffff80>\"{0}\"</color>!", Stats.Theme)));

            if (TutorialEnabled) {
                yield return StartCoroutine(TextBox.Show("Now that you know what the theme is, it's time to pick what kind of game to make!"));
                yield return StartCoroutine(TextBox.Show("The genre you pick will determine your game's <color=#ff80ff>requirements</color>, so choose wisely!"));
            }

            yield return StartCoroutine(GenreBox.Show());

            yield return StartCoroutine(DialogBox.Show(string.Format("I'll try making a {0} this time around!", Stats.Genre.TextName)));

            string name = Genre.RandomName(Stats.Genre.Index);

            yield return StartCoroutine(DialogBox.Show(string.Format("I think I'll call it...")));

            Stats.GameName = name;
            yield return StartCoroutine(DialogBox.Show(string.Format("<color=#80ffff>\"{0}\"</color>!", name)));

            yield return StartCoroutine(DialogBox.Show(string.Format("Time to get working...")));


            if (TutorialEnabled) {
                _timeGlow.SetActive(true);
                yield return StartCoroutine(TextBox.Show("You've only got 48 hours to make your game, so use your time wisely!  (Don't worry, time is paused during menus.)"));
                _timeGlow.SetActive(false);

                _requirementsGlow.SetActive(true);
                yield return StartCoroutine(TextBox.Show("The most important thing to do first is to meet the <color=#ff80ff>requirements</color> for your game genre."));
                _requirementsGlow.SetActive(false);
                _needsGlow.SetActive(true);
                yield return StartCoroutine(TextBox.Show("However, you also need to make sure your <color=#ff8080>needs</color> are met.  Otherwise you'll become less efficient!"));
                _needsGlow.SetActive(false);
                _categoriesGlow.SetActive(true);
                yield return StartCoroutine(TextBox.Show("If you want a really high score, you'll have to make sure that your game performs well in <color=#ffff80>all categories</color>."));
                _categoriesGlow.SetActive(false);
                yield return StartCoroutine(TextBox.Show("That means plenty of <color=#ffff80>design</color> work, interacting with the <color=#ffff80>community</color>, and fixing all of your <color=#ffff80>bugs</color>!"));

                _computerButton.ForceGlow = true;
                yield return StartCoroutine(TextBox.Show("This is your <color=#ffff80>computer</color>.  You can use it for <color=#ffff80>Programming</color>, <color=#ffff80>Art</color>, and <color=#ffff80>Audio</color>, among other things."));
                _computerButton.ForceGlow = false;
                _designButton.ForceGlow = true;
                yield return StartCoroutine(TextBox.Show("In this corner we have your <color=#ffff80>design desk</color>.  You can come up with new <color=#ffff80>Design Ideas</color> for your game here."));
                _designButton.ForceGlow = false;
                _foodButton.ForceGlow = true;
                yield return StartCoroutine(TextBox.Show("And over here are your <color=#ffff80>food supplies</color>.  Make sure you keep yourself healthy!"));
                _foodButton.ForceGlow = false;
                _bedButton.ForceGlow = true;
                _bedButton2.ForceGlow = true;
                yield return StartCoroutine(TextBox.Show("And let's not forget your <color=#ffff80>bed</color>!  You can use it to get a fresh start, though some opt for extreme doses of coffee instead!"));
                _bedButton.ForceGlow = false;
                _bedButton2.ForceGlow = false;

                yield return StartCoroutine(TextBox.Show("That's all you need to know!  Good luck, and see you in 48 hours!"));
            }
        }
        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
    }
}
