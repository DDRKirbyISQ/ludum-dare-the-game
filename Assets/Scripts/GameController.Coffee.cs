﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionCoffee() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("coffee");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("microwave");

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "brewing coffee";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;
        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 3.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    GameController.StopAction(false);

                    AudioManager.PlaySound("slurp");
                    yield return StartCoroutine(DialogBox.Show("*Slurp*..."));
                    if (GameController.Instance.CoffeeCrash) {
                        int energy = UnityEngine.Random.Range(1, 3);
                        GameController.Instance.Stats.CharacterEnergyInternal += energy;
                        yield return StartCoroutine(ResultBox.Show("That didn't really work as well as the last one...",
                            new[] { string.Format("+{0} Energy", energy) },
                            new[] { "icon_energy" }
                        ));
                    } else {
                        int energy = UnityEngine.Random.Range(4, 8);
                        GameController.Instance.Stats.CharacterEnergyInternal += energy;
                        yield return StartCoroutine(ResultBox.Show("I can feel the caffeine surging through my veins!",
                            new[] { string.Format("+{0} Energy", energy) },
                            new[] { "icon_energy" }
                        ));

                        GameController.Instance.CoffeeCrash = true;
                        GameController.Instance.StartCoroutine(GameController.Instance.DoCoffeeCrash());
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
