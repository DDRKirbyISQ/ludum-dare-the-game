﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionBounce() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("bounce");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("bounce");
        AudioManager.FadeMusic(1.0f);

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
            AudioManager.PlayMusic("song1");
        };
        ActionName = "bouncing on the bed";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;
        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 4.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();


                    yield return StartCoroutine(DialogBox.Show("Wheeeeeeeeee!"));
                    if (UnityEngine.Random.value < 0.5f) {
                        int motivation = UnityEngine.Random.Range(3, 6);
                        GameController.Instance.Stats.CharacterMotivationInternal += motivation;
                        yield return StartCoroutine(ResultBox.Show("HECK YEAH!!!!!",
                            new[] { string.Format("+{0} Motivation", motivation) },
                            new[] { "icon_motivation" }
                        ));
                    } else {
                        int energy = UnityEngine.Random.Range(1, 3);
                        GameController.Instance.Stats.CharacterEnergyInternal -= energy;
                        yield return StartCoroutine(ResultBox.Show("That...was a waste of my time.",
                            new[] { string.Format("<color=#ff0000>-{0} Energy</color>", energy) },
                            new[] { "icon_energy" }
                        ));
                    }
                    GameController.StopAction(false);

                    CharacterSprites.Switch("computer");

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
