﻿using System.Collections.Generic;
using UnityEngine;

public class Stats
{
    // Character Stats (Displayed) -- only NeedManager should be changing these.
    public int CharacterFood = 5;

    public int CharacterEnergy = 5;

    public int CharacterMotivation = 5;

    // Character Stats (Internal)
    public float CharacterFoodInternal = 5;

    public float CharacterEnergyInternal = 5;

    public float CharacterMotivationInternal = 5;

    // Amount of work units that the character should do this frame.
    public float CharacterFrameProductivity() {
        return CharacterProductivity() * Time.deltaTime;
    }

    public float CharacterProductivity() {
        // First just get the average number, from 0 to 30.
        float result = (CharacterFood + CharacterEnergy + CharacterMotivation) / 3.0f;

        // Translate into a range from 0.5 to 1.5.
        result = result / 10 * (1.0f) + 0.5f;

        // Penalize by 0.15 for each number that is really bad.
//        if (CharacterFood <= 3) {
//            result -= 0.15f;
//        }
//        if (CharacterEnergy <= 3) {
//            result -= 0.15f;
//        }
//        if (CharacterMotivation <= 3) {
//            result -= 0.15f;
//        }

        // If anything is 0, return 0.
        if (CharacterFood <= 0) {
            return 0.0f;
        }
        if (CharacterEnergy <= 0) {
            return 0.0f;
        }
        if (CharacterMotivation <= 0) {
            return 0.0f;
        }

        return result;
    }

    // Work that's currently in progress.
    public float ProgressCode = 0.0f;

    public float ProgressArt = 0.0f;

    public float ProgressAudio = 0.0f;

    public float ProgressDesign = 0.0f;

    // Game Stats
    public int GameArt = 0;

    public int GameAudio = 0;

    public int GameCommunity = 0;

    public int GameDesign = 0;

    public int GameProgramming = 0;

    public int GameBugs = 0;
        
    // Game Data
    public string GameName = "[Untitled]";

    public string Theme = "???";

    public Genre Genre = null;
}
