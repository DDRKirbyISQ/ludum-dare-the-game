﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionDesign() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("drawing_normal");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("desk");

        Cleanup = () => {
            GameController.Instance.Stats.ProgressDesign /= 2;
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "designing";

        float rand = UnityEngine.Random.value;

        // Always takes some time to start working.
        yield return new WaitForSeconds(kWorkStartDelaySeconds);

        int choice = -1;

        while (true) {
            yield return null;
            GameController.Instance.Stats.ProgressDesign += Time.deltaTime;

            if (rand <= 0.5f) {
                // Normal feature.
                if (GameController.Instance.Stats.ProgressDesign >= 4.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressDesign = 0.0f;
                    GameController.StopAction(false);

                    int design = Mathf.RoundToInt(20 * Stats.CharacterProductivity());
                    Stats.GameDesign += design;
                    yield return ResultBox.Show(
                        "Success!  I have some new design ideas for my game!",
                        new[] { string.Format("+{0} Design", design) },
                        new[] { "icon_design" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            } else if (rand <= 0.75f) {
                if (GameController.Instance.Stats.ProgressDesign >= 3.0f && choice == -1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    yield return StartCoroutine(ChoiceBox.Show("I'm not quite sure how to solve this design problem...", val => {
                        choice = val;
                    }, "Follow conventions", "Do something wild"));
                    if (choice == 0) {
                        yield return StartCoroutine(DialogBox.Show("I'll just do what everyone else does."));
                    } else {
                        yield return StartCoroutine(DialogBox.Show("I'll come up with something that no one will ever expect!"));
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                }

                if (GameController.Instance.Stats.ProgressDesign >= 6.0f && choice == 0) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressDesign = 0.0f;
                    GameController.StopAction(false);

                    int design = Mathf.RoundToInt(25 * Stats.CharacterProductivity());
                    Stats.GameDesign += design;
                    yield return ResultBox.Show(
                        "It's not exciting, but it works!",
                        new[] { string.Format("+{0} Design", design) },
                        new[] { "icon_design" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
                if (GameController.Instance.Stats.ProgressDesign >= 6.0f && choice == 1) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressDesign = 0.0f;
                    GameController.StopAction(false);

                    if (UnityEngine.Random.value < 0.5f) {
                        int design = Mathf.RoundToInt(35 * Stats.CharacterProductivity());
                        int motivation = UnityEngine.Random.Range(1, 3);
                        Stats.GameDesign += design;
                        Stats.CharacterMotivationInternal += motivation;
                        yield return ResultBox.Show(
                            "That really worked out well for me!",
                            new[] {
                                string.Format("+{0} Design", design),
                                string.Format("+{0} Motivation", motivation)
                            },
                            new[] { "icon_design", "icon_motivation" }
                        );

                    } else {
                        int design = Mathf.RoundToInt(10 * Stats.CharacterProductivity());
                        Stats.GameDesign += design;
                        yield return ResultBox.Show(
                            "...it didn't really work that well...",
                            new[] { string.Format("+{0} Design", design) },
                            new[] { "icon_design" }
                        );
                    }

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }


            } else {

                // Quick feature.
                if (GameController.Instance.Stats.ProgressDesign >= 2.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    Stats.ProgressDesign = 0.0f;
                    GameController.StopAction(false);

                    int design = Mathf.RoundToInt(10 * Stats.CharacterProductivity());
                    Stats.GameDesign += design;
                    yield return ResultBox.Show(
                        "I came up with a quick, small idea!",
                        new[] { string.Format("+{0} Design", design) },
                        new[] { "icon_design" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }

            }
        }
    }
}
