﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class RequirementsText : MonoBehaviour
{
    Text _text;

    void Awake() {
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        StringBuilder builder = new StringBuilder("Requirements:\n");

        if (GameController.Instance.Stats.Genre == null) {
            builder.Append("???");
        } else {
            foreach (Genre.Requirement requirement in GameController.Instance.Stats.Genre.Requirements) {
                builder.Append(requirement.FormattedText() + "\n");
            }
            _text.text = GameController.Instance.Stats.Genre.Name;
        }

        _text.text = builder.ToString();
    }
}
