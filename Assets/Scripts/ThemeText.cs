﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeText : MonoBehaviour
{
    Text _text;

    void Awake() {
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        _text.text = string.Format("Theme: {0}", GameController.Instance.Stats.Theme);
    }
}
