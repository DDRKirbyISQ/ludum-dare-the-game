﻿using System.Text;
using System;
using System.Collections;
using System.Collections.Generic;
using HarmonicUnity;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceBox : GenericBox
{
    public static ChoiceBox Instance;

    [SerializeField]
    Text _choice0;

    [SerializeField]
    Text _choice1;

    [SerializeField]
    GameObject[] _buttons;

    int _selectedChoice = -1;

    public static IEnumerator AskStopAction(Action<int> action) {
        if (GameController.Instance.ActionName == "") {
            action(1);
            yield break;
        }

        yield return Show("I'll have to stop " + GameController.Instance.ActionName + "...is that okay?", action);
        yield break;
    }

    public static IEnumerator Show(string text, Action<int> action, string choice0 = "No", string choice1 = "Yes") {
        Instance._choice0.text = choice0;
        Instance._choice1.text = choice1;
        Instance._selectedChoice = -1;
        Instance.gameObject.SetActive(true);
        foreach (GameObject button in Instance._buttons) {
            button.SetActive(false);
        }
        yield return Instance.StartCoroutine(Instance.ShowText(text));

        foreach (GameObject button in Instance._buttons) {
            button.SetActive(true);
        }
        while (Instance._selectedChoice == -1) {
            yield return null;
        }

        yield return null;
        Instance.gameObject.SetActive(false);
        action(Instance._selectedChoice);
        yield break;
    }

    public void SelectChoice(int choice) {
        _selectedChoice = choice;
    }

    protected override void Awake() {
        base.Awake();
        Instance = this;
    }
}
