﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public class NeedManager : MonoBehaviour
{
    float foodMult = 1.0f;

    float energyMult = 1.0f;

    float motivationMult = 1.0f;

    void Awake() {
        foodMult = 1.2f;
        energyMult = 0.9f;
        motivationMult = 1.0f;
    }

    // Update is called once per frame
    void Update() {
        float amount = Time.deltaTime / 10.0f;
        GameController.Instance.Stats.CharacterFoodInternal -= amount * foodMult;
        GameController.Instance.Stats.CharacterEnergyInternal -= amount * energyMult;
        GameController.Instance.Stats.CharacterMotivationInternal -= amount * motivationMult;

        GameController.Instance.Stats.CharacterFoodInternal = Mathf.Clamp(GameController.Instance.Stats.CharacterFoodInternal, 0, 11);
        GameController.Instance.Stats.CharacterEnergyInternal = Mathf.Clamp(GameController.Instance.Stats.CharacterEnergyInternal, 0, 11);
        GameController.Instance.Stats.CharacterMotivationInternal = Mathf.Clamp(GameController.Instance.Stats.CharacterMotivationInternal, 0, 11);

        float prevProductivity = GameController.Instance.Stats.CharacterProductivity();

        int prevFood = GameController.Instance.Stats.CharacterFood;
        int newFood = Mathf.RoundToInt(GameController.Instance.Stats.CharacterFoodInternal);
        newFood = Mathf.Clamp(newFood, 0, 10);
        GameController.Instance.Stats.CharacterFood = newFood;
        int prevEnergy = GameController.Instance.Stats.CharacterEnergy;
        int newEnergy = Mathf.RoundToInt(GameController.Instance.Stats.CharacterEnergyInternal);
        newEnergy = Mathf.Clamp(newEnergy, 0, 10);
        GameController.Instance.Stats.CharacterEnergy = newEnergy;
        int prevMotivation = GameController.Instance.Stats.CharacterMotivation;
        int newMotivation = Mathf.RoundToInt(GameController.Instance.Stats.CharacterMotivationInternal);
        newMotivation = Mathf.Clamp(newMotivation, 0, 10);
        GameController.Instance.Stats.CharacterMotivation = newMotivation;

        float newProductivity = GameController.Instance.Stats.CharacterProductivity();

        if (newProductivity < 0.75f && prevProductivity >= 0.75f) {
            StartCoroutine(ShowProductivity1());
        } else if (newProductivity > 1.25f && prevProductivity <= 1.25f) {
            StartCoroutine(ShowProductivity2());
        } else if (newFood < 4 && prevFood >= 4) {
            StartCoroutine(ShowFood1());
        } else if (newEnergy < 4 && prevEnergy >= 4) {
            StartCoroutine(ShowEnergy1());
        } else if (newMotivation < 4 && prevMotivation >= 4) {
            StartCoroutine(ShowMotivation1());
        } else if (newFood < 1 && prevFood >= 1) {
            StartCoroutine(ShowDead1());
        } else if (newEnergy < 1 && prevEnergy >= 1) {
            StartCoroutine(ShowDead1());
        } else if (newMotivation < 1 && prevMotivation >= 1) {
            StartCoroutine(ShowDead1());
        }
    }

    IEnumerator ShowProductivity1() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        if (Random.value < 0.5f) {
            yield return DialogBox.Show("I'm not feeling quite as productive as I was earlier...");
        } else {
            yield return DialogBox.Show("My productivity is dropping...I should try to take better care of myself!");
        }

        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
        yield break;
    }

    IEnumerator ShowProductivity2() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        if (Random.value < 0.5f) {
            yield return DialogBox.Show("I'm really feeling confident about things!");
        } else {
            yield return DialogBox.Show("I feel amazingly productive right now!");
        }

        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
        yield break;
    }

    IEnumerator ShowFood1() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        if (Random.value < 0.5f) {
            yield return DialogBox.Show("I'm really starting to feel hungry...");
        } else {
            yield return DialogBox.Show("I can't concentrate on an empty stomach...I should eat some food...");
        }

        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
        yield break;
    }

    IEnumerator ShowEnergy1() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        if (Random.value < 0.5f) {
            yield return DialogBox.Show("I'm starting to lose focus...I need some sleep...");
        } else {
            yield return DialogBox.Show("I'm getting pretty tired...I could use some rest...");
        }

        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
        yield break;
    }

    IEnumerator ShowMotivation1() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        if (Random.value < 0.5f) {
            yield return DialogBox.Show("I don't feel like working anymore...I should do something fun...");
        } else {
            yield return DialogBox.Show("I'm not feeling very motivated...maybe I should try taking my mind off of things...");
        }

        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
        yield break;
    }

    IEnumerator ShowDead1() {
        yield return DefaultCanvas.GetDialogLock();
        GameController.Pause();

        if (Random.value < 0.5f) {
            yield return DialogBox.Show("I can't go on like this...");
        } else {
            yield return DialogBox.Show("I feel terrible.");
        }

        DefaultCanvas.ReleaseDialogLock();
        GameController.Unpause();
        yield break;
    }
}
