﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultItem : MonoBehaviour
{
    [SerializeField]
    Text _text;

    [SerializeField]
    Image _image;

    public void Set(string text, string icon) {
        Sprite sprite = Resources.Load<Sprite>(icon);
        _image.sprite = sprite;
        _text.text = text;
    }
}
