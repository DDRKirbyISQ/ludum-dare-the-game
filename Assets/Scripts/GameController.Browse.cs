﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HarmonicUnity;

public partial class GameController : MonoBehaviour
{
    IEnumerator GameActionBrowse() {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>("typing_normal");
        audioSource.loop = true;
        audioSource.Play();
        CharacterSprites.Switch("computer");

        Cleanup = () => {
            audioSource.Stop();
            Destroy(audioSource);
        };
        ActionName = "browsing";

        float rand = UnityEngine.Random.value;

        float progress = 0.0f;
        while (true) {
            yield return null;
            progress += Time.deltaTime;

            if (rand <= 1.0f) {
                // Normal feature.
                if (progress >= 5.0f) {
                    yield return DefaultCanvas.GetDialogLock();
                    GameController.Pause();

                    GameController.StopAction(false);

                    int motivation = UnityEngine.Random.Range(1, 4);
                    int design = Mathf.RoundToInt(5 * Stats.CharacterProductivity());
                    Stats.GameDesign += design;
                    Stats.CharacterMotivationInternal += motivation;
                    yield return ResultBox.Show(
                        "I think I have some new ideas from reading those posts!",
                        new[] {
                            string.Format("+{0} Design", design),
                            string.Format("+{0} Motivation", motivation)
                        },
                        new[] { "icon_design", "icon_motivation" }
                    );

                    DefaultCanvas.ReleaseDialogLock();
                    GameController.Unpause();
                    yield break;
                }
            }
        }
    }
}
